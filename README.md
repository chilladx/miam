# MIAM

Mes Idées À Manger

## Description

Partage de recettes de cuisine.

## Installation

Pas besoin d'installer quoi que ce soit, le site est disponible [ici](https://miam.mondrieux.com/).
Le site est basé sur [Hugo](https://gohugo.io/). Pour du développement, il suffit de cloner le repository, et d'utiliser le serveur local :

```sh
$ cd miam
$ hugo server
```

## Contribution

Vous êtes bienvenus pour partager vos recettes ! Il suffit de créer une branche, et d'ajouter un fichier markdown dans le dossier des posts : [content/posts](content/posts).

Vos posts doivent contenir l'entête suivante :

```
+++
title = <TITRE>
date = <DATE FORMAT:YYYY-MM-DD
author = <AUTEUR|AUTRICE>
tags = [
    <TAG>,
    <[TAG>,]
]
categories = [
    <CATEGORIE>,
    [<CATEGORIE>,]
]
+++
```

## Auteurs/autrices et remerciements

La bannière du site est de [May Gauthier](https://unsplash.com/@maygauthier?utm_content=creditCopyText&utm_medium=referral&utm_source=unsplash) sur [Unsplash](https://unsplash.com/photos/a-magazine-sitting-on-top-of-a-wooden-table-sRevlaFs2u4?utm_content=creditCopyText&utm_medium=referral&utm_source=unsplash).

Le thème est [simplog](https://github.com/michimani/simplog) par [Yoshihiro Ito](https://github.com/michimani)

## License

MIT, voir [LICENSE](LICENSE)
