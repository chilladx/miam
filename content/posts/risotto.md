+++
title = 'Risotto classique'
date = 2023-11-13
author = "Alain Andrieux"
rating = 5
tags = [
    "riz",
    "catégorie: plat",
]
categories = [
    "plat",
]
+++

## Ingrédients

(4 personnes)

- 1 gros oignon
- 70 g de beurre
- 200 ml de vin blanc sec
- 1200 ml de bouillon clair (volaille, veau, bœuf)
- 350 g de riz italien à grains ronds (Arborio ...)
- 150 g de gorgonzola (sans mascarpone)
- 150 ml de crème fraîche
- Sel, poivre

## Recette

1. Peler l’oignon, le hacher finement. Le faire revenir dans 50 g de beurre jusqu’à ce qu’il devienne transparent.
2. 1. Pendant ce temps, porter le bouillon à ébullition.
3. Déglacer l’oignon avec du vin. Laisser réduire un peu.
4. Ajouter alors le riz non lavé. Verser une louche de bouillon dans le riz et, à partir de ce moment là, remuer constamment.
5. Cuire le riz à feu moyen, Veiller à ce qu’il n’attache pas.
6. Rajouter une nouvelle louche de bouillon chaud dès que la précédente est absorbée.
7. Continuer à remuer et à ajouter du bouillon jusqu’à ce que le riz soit tendre, mais encore ferme sous la dent.
8. Il ne doit être ni gorgé d’eau, ni trop sec, et surtout ne pas tourner à la bouillie.
9. Compter pour cela entre 20 et 30 min.
10. Commencer à goûter au bout de 20 min. S’il n’y a plus de bouillon, utiliser de l’eau chaude.
11. Après 20 min, incorporer le gorgonzola coupé en tranches Lorsque le riz est cuit, mais encore ferme, ajouter pour terminer la crème, le reste du beurre et un peu de bouillon.
