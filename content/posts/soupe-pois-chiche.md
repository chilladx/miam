+++
title = 'Soupe Pois Chiche'
date = 2023-11-07
author = "Julien Andrieux"
rating = 5
tags = [
    "pois chiche",
    "carotte",
    "catégorie: soupe",
]
categories = [
    "soupe",
]
+++

## Ingrédients

- 1 gros oignon
- 4 ou 5 carottes
- 1 ou 2 navets
- 1 boite de pois chiche (~250gr)

## Recette

1. Dorer les oignons dans une cocotte, avec une cuillère à soupe d'huile d'olive
2. Ajouter les carottes et les navets coupés en dés, les faire sauter 5 min
3. Ajouter une cuillère à soupe de concentré de tomate
4. Couvrir d'eau et laisser cuire à petits bouillons pendant 20 minutes
5. Ajouter les pois chiches égouttés, laisser cuire 10 minutes supplémentaires
6. Mixer en allongeant à l'eau pour obtenir la texture désirée
