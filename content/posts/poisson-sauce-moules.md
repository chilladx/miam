+++
title = 'Poisson sauce aux moules'
date = 2023-11-13
author = "Alain Andrieux"
rating = 5
tags = [
    "poisson",
    "moule",
    "catégorie: plat",
]
categories = [
    "plat",
]
+++

## Ingrédients

(4 personnes)

- 1 kg de moules
- filets de cabillau (200g / personne)
- champignons de Paris (petite boite)

## Recette

1. Nettoyer les moules et les mettre à ouvrir dans un faitout sans ajouter d’eau. Sortir les moules des coquilles, réserver le jus du faitout (l’eau des moules).
2. Mettre le poisson dans l’eau froide (juste couvrir) et amener à frémissement. Couper le feu.
3. Faire une sauce blanche (matière grasse, farine [ 2 cueillérées], eau de cuisson du poisson), ajouter les champignons de Paris égoutés, les moules et ajuster la consistance avec l’eau des moules.
4. Disposer le poisson dans le plat de service, et napper avec la sauce. Accompagner de riz.
