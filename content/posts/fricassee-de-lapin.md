+++
title = 'Fricassée de lapin façon Alain'
date = 2023-11-13
author = "Alain Andrieux"
rating = 5
tags = [
    "lapin",
    "catégorie: plat",
]
categories = [
    "plat",
]
+++

## Ingrédients

(2-3 personnes)

- Rable de lapin découpé (500 g)
- Champignons de Paris (300 g)
- Huile d’olive
- Crème fraiche épaisse, moutarde
- Vin blanc sec (10 cl)
- Ail, herbes de Provence
- Sel, poivre

## Recette

1. Dans une cocotte, faire revenir les morceaux de lapin de tous cotés dans l’huile chaude.
2. Quand tous les morceaux sont bien dorés, saler, poivrer, ajouter l’ail haché et les champignons lavés et coupés en morceaux.
3. Ajouter le vin blanc, remuer et laisser cuire à feu doux pendant 20 minutes.
4. A mi-cuisson ajouter une cuillère à soupe de crème fraiche mélangée à une cuillère de moutarde. Remuer.
5. Servir avec des pâtes fraiches ou du riz complet.
