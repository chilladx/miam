+++
title = 'Boeuf bourguignon'
date = 2023-11-12
author = "Julien Andrieux"
rating = 5
tags = [
    "boeuf",
    "lard",
    "catégorie: plat",
    "catégorie: mijoté",
]
categories = [
    "mijoté",
    "plat",
]
+++

## Ingrédients

pour 4 à 6 personnes

- 1 kg de boeuf (paleron ou macreuse) coupé en cubes de 40 g
- Huile d'olive
- 35 g de beurre coupé en dés
- 120 g de lard paysan coupé en lardons de 1 cm
- 12 oignons grelots pelés
- 180 g de petits champignons de Paris pelés
- Sel et poivre mignonnette
- 1 gros oignon coupé en petits dés
- 1 carotte coupée en petits dés
- 2 gousses d'ail hachées
- 1 c. à s. de farine
- 1 L de vin rouge de Bourgogne
- 1 tablette de jus de veau
- 1 bouquet garni

## Recette

1. Faites marinner le boeuf dans le vin avec du sel et du poivre pendant 24h.
2. Faites chauffer 1,5 c. à s. d'huile dans la cocotte avec 5g de beurre. Faites-y revenir les lardons 1 min à feu vif. Ajoutez les oignons grelots, les champignons et 10g de beurre. Remuez et laissez revenir 5 min. Versez le tout dans le panier vapeur posé sur une assiette.
3. Salez la viande de tous les cotés et parsemez-la de 1 c. à c. de poivre mignonnette. Faites chauffer 1/2 c. à s. d'huile dans la cocotte avec 20g de beurre. Faites-y rissoler la viande 4-5 min à feu moyen, en la retournant plusieurs fois. Ajoutez l'oignon, la carotte et l'ail. Laissez revenir 2 min. Saupoudrez avec la farine et remuez.
4. Versez le vin dans la cocotte avec la tablette de jus de veau, le bouquet garni et le contenu du panier vapeur. Mélangez.
5. Fermez la cocotte. Dès que la vapeur s'échappe, baissez le feu et laissez cuire 35 min.
6. Retirez le bouquet garni de la cocotte et servez chaud, accompagné de tagliatelles, en décorant de persil haché.
