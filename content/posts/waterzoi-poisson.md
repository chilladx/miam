+++
title = "Waterzoï de poissons"
date = 2023-11-13
author = "Alain Andrieux"
rating = 5
tags = [
    "poisson",
    "catégorie: plat",
]
categories = [
    "plat",
]
+++

## Ingrédients

(6 personnes)

- 1,5 kg de poissons divers (saint-pierre, dorade, merlan, congre, grondin)
- 1 l de moules
- 1/2 pied de céleri-rave, 2 blancs de poireaux
- 2 oignons, 3 carottes
- 1 bouquet garni
- 125 g de crème
- 2 oeufs, 1 citron
- 60 g de beurre
- 3 cuil à soupe de farine
- sel, poivre, muscade

## Recette

1. Le waterzoï est une soupe de poissons flamande qui se fait avec des poissons à chair blanche tels que : saint-pierre, dorade, merlan, congre, grondin.
2. Faites enlever la peau des poissons et détacher les filets. Réservez toutes les parures pour en faire un fumet avec 2 litres d'eau peu salée, oignons et bouquet garni. Faites cuire à grand feu 30 minutes couvert. Faites ouvrir les moules avec un petit verre d'eau salée. Sortez-les des coquilles, décantez et réservez l'eau. Coupez les légumes en fine julienne et faites-les fondre doucement au beurre dans une cocotte assez grande pour contenir le bouillon et les poissons. Dès qu'ils blondissent, couvrez la cocotte Laissez étuver à petit feu pendant 15 minutes.
3. Ajoutez les poissons coupés en morceaux, mouillez avec le fumet passé et l'eau des moules. Mélangez la crème, les jaunes des œufs, la farine et le jus de la moitié du citron. Versez dans la cocotte, poivrez, muscadez, salez prudemment. Amenez à ébullition en remuant, baissez le feu et laissez frémir 20 minutes. Ajoutez les moules au moment de servir.
4. Servez le bouillon et les poissons dans des assiettes creuses. Mettez sur la table des tranches de pain de campagne grillées et du beurre.

Nous vous recommandons de servir un Pouilly-sur-Loire avec le waterzoï.
