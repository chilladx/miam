+++
title = 'Fondue savoyarde'
date = 2023-11-13
author = "Alain Andrieux"
rating = 5
tags = [
    "fromage",
    "fondue",
    "catégorie: plat",
]
categories = [
    "plat",
]
+++

## Ingrédients

(6 personnes)

- 300 g de Beaufort
- 300 g de Comté
- 300 g de Fribourg
- 300 g de Aprentzel
- 1 gousse d’ail
- 2 c. à s. de kirsch
- 1 pain de campagne d’1 kg
- 1 bouteille de vin blanc de Savoie (Apremeont)

## Recette

1. Frotter un caquelon avec la gousse d’ail épeluchée.
2. Couper les fromages en petits dés et les mélanger.
3. Mettre un verre de vin dans le caquelon et faire chauffer à feu doux.
4. Quand le vin arrive à ébullition, mettre un peu de fromage à fondre en remuant pour éviter les grumauds.
5. En procédant par petites quantités, ajouter tout le fromage en rajoutant du vin si nécessaire.
6. Lorsque tout le fromage est fondu en une pâte homogène, ajouter le kirsch hors du feu et disposer le caquelon à table sur un réchaud à alcool.
7. Présenter le pain coupé en morceaux.

Accompagner de vin blanc d’Apremont.
