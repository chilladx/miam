+++
title = 'Roti de porc façon Alex'
date = 2023-11-12
author = "Julien Andrieux"
rating = 5
tags = [
    "porc",
    "lard",
    "catégorie: plat",
    "catégorie: roti",
]
categories = [
    "roti",
    "plat",
]
+++

## Ingrédients

Pour 6 à 8 personnes (ou 3 adx) :

- 1,5 kg de roti de porc (échine) ;
- 1 kg de tomates coupées en quartier ;
- 400 gr de champignons de Paris coupés en tranche ;
- 3 gros oignons coupés en lamelle ;
- 30 cL d'eau ;
- 1 concentré de tomate ;
- 3 gousses d'ail finement hachées ;
- Persil, sel, poivre.

# Recette

1. Faire chauffer une noix de beurre avec 2 c. à s. d'huile d'olive dans la cocotte à feu vif. Y faire blondir les oignons pendant 3 min. Quand les oignons se colorent, ajouter les champignons, et arroser généreusement de persil. Laisser réduire en remuant très régulièrement, pendant 5 min. Pendant ce temps, poivrer généreusement la viande sur tous les cotés. Réserver.
2. Remettre une noix de beurre avec 2 c. à s. d'huile d'olive dans la cocotte. Y déposer le roti, et le colorer de tous les cotés.
3. Ajouter l'eau, le concentré de tomate, l'ail, du sel et du poivre, et remuer. Ajouter les champignons et les oignons, ainsi que les quartiers de tomate. Saupoudrer de persil, remuer, et fermer la cocotte en la laissant sur le feu vif. Une fois que la soupape tourne, ramener le feu à moyen, et laisser cuire 60 min.
