+++
title = "Tagine de mouton aux légumes"
date = 2023-11-13
author = "Alain Andrieux"
rating = 5
tags = [
    "mouton",
    "catégorie: plat",
]
categories = [
    "plat",
]
+++

## Ingrédients

- 1kg de mouton gras
- 200g de carottes
- 200g de navets
- 200g d'oignons
- 200g de courgettes
- 200g de tomates
- 200g de raisins secs
- 60g d'huile d'olive
- piments fort (Cayenne) et doux (paprika)
- sel - poivre

## Recette

1. Dans une cocotte en fonte épaisse avec couvercle...
2. Couper la viande en morceaux. Faire revenir dans l'huile avec sel poivre, puis ajouter les deux piments.
3. Ajouter les carottes coupées en long et les navets coupés en morceaux. Couvrir et laisser mijoter 30 minutes (ne pas ajouter d'eau).
4. Faire tremper les raisins secs dans de l’eau tiède durant environ 30 minutes..
5. Mettre les oignons, tomates, courgettes coupées en morceaux et les raisins secs.
6. Laisser cuire à nouveau 30 minutes.
