+++
title = 'Osso buco'
date = 2023-11-12
author = "Julien Andrieux"
rating = 5
tags = [
    "veau",
    "catégorie: plat",
]
categories = [
    "plat",
]
+++

## Ingrédients

pour 4 personnes

- 4 tranches de jarret de veau
- 2 c. à s. d'huile de pépins de raisin
- 20g de beurre
- Sel et poivre du moulin
- 1 gros oignon coupé en dés
- 2 tomates coupées en huit, pelées et épépinées
- 1 c. à s. de concentré de tomate
- 15 cl de vin blanc
- 40 cl de jus de veau (avec 2 tablettes)
- 4 feuilles de sauge fraîche
- Le zeste d'1/2 orange non traitée, en petits rubans
- 1 bouquet garni

## Recette

1. Faites chauffer 2 c. à s. d'huile à feu vif dans la cocotte. Déposez les tranches de veau et répartissez 10 g de beurre des deux cotés de la cocotte. Poivrez généreusement, salez et laissez dorer 3 min. Retournez la viande. Salez poivrez et ajoutez 10g de beurre et laissez dorer encore 3 min. Déposez le veau sur un plat.
2. Faites blondir l'oignon 1 min à feu moyen dans la cocotte. Ajoutez les tomates, salez et poivrez. Laissez revenir 1 min. Ajoutez le concentré de tomate. Remuez et laissez revenir 1 min.
3. Remettez la viande dans la cocotte avec le jus qu'elle a rendu. Arrosez de vin blanc et laissez réduire de moitié pendant environ 3 min. Ajoutez le jus de veau, la sauge, les zestes d'orange et le bouquet garni. Portez à ébullition sans couvrir.
4. Fermez la cocotte. Dès que la vapeur s'échappe, baissez le feu et laissez cuire pendant 20 min.
5. Ôtez le bouquet garni de la cocotte et servez bien chaud.
