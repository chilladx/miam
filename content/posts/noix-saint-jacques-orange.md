+++
title = "Noix de Saint-Jacques à l'orange, endives braisées"
date = 2023-11-13
author = "Alain Andrieux"
rating = 5
tags = [
    "saint-jacques",
    "endives",
    "catégorie: plat",
]
categories = [
    "plat",
]
+++

## Ingrédients

(4 personnes)

- 16 noix de Saint-Jacques
- 4 oranges
- 200g de beurre
- 10cl de crème liquide
- 5cl de vin blanc
- 1 échalotte
- 60g sucre en poudre
- 4 endives
- 1 oignon
- sel, poivre

## Recette

1. Nettoyer les St-Jacques et les mettre au frigo sur du papier absorbant.
2. Beurre d’orange : presser 2 oranges pour en extraire le jus, prélever le zeste des deux autres. Blanchir les zestes dans un peu d’eau, egoutter et passer aussitôt sous l’eau froide. Peler à vif les deux oranges sans zeste puis récupérer la chair entre les membranes blanches. Garder quelques quartiers pour la déco. Hacher finement l’échalotte ; en verser la moitié dans une casserole avec le vin blanc, le jus des oranges et le sucre en poudre. Porter à ébullition puis laisser réduire doucement des 2/3.
3. Préparer les endives braisées (1 hr de cuisson, voir la recette)
4. Saler et poivrer les noix de St-Jacques, les saisir dans un peu d’huile d’olive très chaude avec une noisette de beurre. Baisser le feu et laisser cuire doucement pour qu’elles soient dorées et moelleuses.
5. Reprendre la préparation au beurre d’oranges et la faire chauffer avec 10cl de crème. Ajouter le beurre restant, la moitié des zestes et monter le ‘beurre blanc’ hors du feu et fouettant bien.
6. Déposer les noix de St-Jacques et les chicons sur le beurre d’orange ; décorer les chicons avec les zestes d’oranges et les noix de St-Jacques avec des oeufs de lump.
