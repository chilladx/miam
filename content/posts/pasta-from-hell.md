+++
title = "Pasta from Hell"
date = 2023-11-13
author = "Alain Andrieux"
rating = 5
tags = [
    "pate",
    "catégorie: plat",
]
categories = [
    "plat",
]
+++

## Ingrédients

(4 personnes)

- 500g de pâtes (macaroni, tagliatelles, etc...)
- 750g champignons de Paris
- 2 gros oignons blancs
- 4 fines tranches de jambon blanc
- 1 tomate bien ferme
- 2 jaunes d’œuf
- beurre, sel, poivre, ail, tabasco
- épices au choix (basilic, coriandre, persil,aneth)

## Recette

1. Faire suer les champignons de Paris lavés et coupés en morceaux (têtes et pieds) à la poële. Mettre de côté.
2. Faire dorer les oignons dans une noix de beurre. Ajouter les champignons quelques instants et remuer.
3. Faire cuire les pâtes “al dente” dans beaucoup d’eau salée. Égoutter.
4. Dans une grande casserole ou un faitout, faire fondre une grosse noix de beurre, y verser les pâtes et remuer pour qu’elles deviennent légèrement colorées et brillantes.
5. Ajouter le mélange champignons et oignons, ainsi que le jambon coupé en fines lanières et la tomate coupée en tout petits morceaux.Remuer.
6. Ajouter les jaunes d’œuf, les épices et un trait de tabasco (ou plusieurs) selon le goût.
7. Disposer dans un plat de service en veillant à la présentation.
